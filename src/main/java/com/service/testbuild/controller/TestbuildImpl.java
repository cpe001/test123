package com.service.testbuild.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-25T03:28:51.101Z")

@RestSchema(schemaId = "testbuild")
@RequestMapping(path = "/testbuild", produces = MediaType.APPLICATION_JSON)
public class TestbuildImpl {

    @Autowired
    private TestbuildDelegate userTestbuildDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userTestbuildDelegate.helloworld(name);
    }

}
